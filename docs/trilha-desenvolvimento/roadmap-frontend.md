---
sidebar_position: 2
title: Trilha de Front-End
---

## Objetivo Geral:  
Aprender conceitos de front-end com Vue, frameworks como Vuetify, e boas práticas de desenvolvimento.


## 1. Introdução ao Vue.js  (3h)
Objetivo: Entender a estrutura e funcionamento do framework Vue.js.  
- Tarefas:
  - Estudar o sistema de componentes do Vue.
  - Compreender o ciclo de vida dos componentes.
  - Trabalhar com diretivas (`v-for`, `v-if`, `v-model`, etc.).
  - Praticar a reatividade e entender o uso de `ref`, `reactive`, e `computed`.
  - Entender sobre DOM e como funciona o interpretador.
- Referências:

- [[Vídeo Aula] - Introdução sobre Vue.js](https://www.youtube.com/watch?v=LCSQK_Gcvnk) (15 min)
 
- [[Documentação] - Mecanismo de Interpretação](https://www.youtube.com/watch?v=LCSQK_Gcvnk) (25 min)

## 2. Arquitetura de Projetos Vue.js  (2h)
Objetivo: Organizar um projeto Vue.js corretamente.  
- Tarefas:
  - Estruturar o projeto usando boas práticas de organização de pastas e arquivos.
  - Explorar a separação de lógica (composição de componentes, reutilização de código).
  - Implementar roteamento com `vue-router`.
  - Criar estados globais com `Vuex` ou `Pinia` para gerenciar dados complexos.
- Referências:
  - [[Documentação] - Guia de estrutura - Vuex](https://vuex.vuejs.org/ptbr/guide/structure.html) (30 min)


## 3. Vuetify: Interface de Usuário (UI) (50 min) 
Objetivo: Desenvolver UIs modernas e responsivas utilizando Vuetify.  
- Tarefas:
  - Entender o funcionamento do grid system e layout responsivo do Vuetify.
  - Explorar e customizar componentes de interface: tabelas, botões, modais, formulários.
  - Implementar temas e customização de design.
  - Praticar com componentes avançados (como `v-data-table`, `v-dialog`, `v-select`).
- Referências:
  - [[Documentação] - Dialog component — Vuetify](https://vuetifyjs.com/en/components/dialogs/#usage) (25 min)
  - [[Documentação] - Select component — Vuetify](https://vuetifyjs.com/en/components/selects/#usage) (25 min)


## 4. Boas Práticas com TypeScript (1:20h) 
Objetivo: Utilizar TypeScript para adicionar tipagem estática e aumentar a confiabilidade do código.  
- Tarefas:
  - Definir corretamente as tipagens para dados e componentes.
  - Aprender sobre interfaces, tipos, e a integração com o Vue 3 (`script setup` com `lang="ts"`).
  - Refatorar código para usar TypeScript de maneira eficiente.
- Referências:
  - [[Tutorial] - Refactoring Vue 3 code | Reusable Form Component | Reusable Components](https://www.youtube.com/watch?v=rBDBXzInFmc) (20 min)

## 5. Gerenciamento de Estado e APIs (2:30h)
Objetivo: Sincronizar o front-end com APIs, manipulando dados de maneira eficiente.  
- Tarefas:
  - Integrar o front-end com APIs utilizando `axios` ou `fetch`.
  - Manipular respostas de APIs assíncronas (async/await).
  - Implementar paginação e filtros em tabelas com base em dados da API.
  - Estudar controle de erro e feedback ao usuário (ex: `Swal` para mostrar alertas de sucesso ou falha).
- Referências:
  - [[Tutorial] - Vue JS 3 Tutorial for Beginners #6 - Using Axios to Consume APIs in Vue 3](https://www.youtube.com/watch?v=7BoUqFq31oI) (15 min)
  - [[Documentação] - Usando Axios para Consumir APIs — Vue.js:](https://br.vuejs.org/v2/cookbook/using-axios-to-consume-apis.html) (15 min)


## 6. Componentização e Reutilização de Código (1:15h) 
Objetivo: Reutilizar componentes para aumentar a eficiência no desenvolvimento.  
- Tarefas:
  - Criar componentes base (ex: `BaseBreadcrumb`, tabelas genéricas) para evitar repetição de código.
  - Desenvolver lógica reutilizável com `mixins` ou `composables`.
  - Refatorar o projeto para dividir funcionalidades em componentes menores e mais reutilizáveis.
- Referências:
  - [[Documentação] - Breadcrumbs component — Vuetify](https://vuetifyjs.com/en/components/breadcrumbs/#usage) (15 min)
  

## 7. Melhorias de Desempenho e Otimização  (1:50h)
Objetivo: Melhorar o desempenho da aplicação Vue.js.  
- Tarefas:
  - Implementar lazy loading de componentes e roteamento.
  - Utilizar técnicas de memoization para evitar recomputações desnecessárias.
  - Estudar a otimização de tabelas grandes com paginação e filtragem eficiente.
  - Utilizar funções assíncronas para carregamento assíncrono.
- Referências:
  - [[Documentação] - Lazy component — Vuetify](https://vuetifyjs.com/en/components/lazy/#usage) (25 min)
  - [[Documentação] - Pagination component — Vuetify](https://vuetifyjs.com/en/components/paginations/#usage) (25 min)

## 8. Testes e Qualidade de Código (2:45h) 
Objetivo: Garantir a qualidade do código com testes e ferramentas de linting.  
- Tarefas:
  - Configurar linting e formatação automática com ESLint e Prettier.
  - Realizar testes E2E com Cypress ou Playwright.
- Referências:
  - [[Documentação] - Testes do Sistema SLAVE ONE](https://dev.to/marcela_lage_094e814c6a4e/documentacao-dos-testes-do-sistema-slave-one-2kmb) (30 min)
  - [[Documentação] - Fixtures do Cypress para testes](https://dev.to/gustavoacaetano/fixtures-do-cypress-para-testes-1748) (15 min)
