---
sidebar_position: 1
title: Trilha de Back-End
---

## 1. GIT e GitHub (1:30h)

O Git é um sistema de controle de versão distribuído amplamente utilizado para gerenciar o histórico de alterações de arquivos em projetos de desenvolvimento de software. Essa ferramenta permite que múltiplos desenvolvedores trabalhem de forma colaborativa, rastreando mudanças, revertendo para versões anteriores e mantendo um histórico completo de todas as modificações realizadas. 

O GitHub é uma plataforma de hospedagem de código-fonte que utiliza o Git como sistema de controle de versão.

[[Vídeo Aula] - Aprenda Git e Github do zero](https://www.youtube.com/watch?v=pyM5QLS2h6M)

[[Vídeo Aula] - Curso de Git e Github (Desafios e Práticas)](https://www.youtube.com/watch?v=kB5e-gTAl_s)

## 2. Introdução ao C# (4h)

C# é uma linguagem de programação moderna, orientada a objetos e fortemente tipada, desenvolvida pela Microsoft como parte da plataforma .NET. Foi projetada para ser simples, eficiente e robusta, combinando a facilidade de uso de linguagens como Visual Basic com o poder e a flexibilidade de linguagens como C++ e Java.

[[Vídeo Aula] - Curso completo de c#](https://www.youtube.com/watch?v=wxznTygnRfQ) (4h)

**Leitura complementar (30 min):** 
- [[Documentação] - List, ICollection, IEnumerable..](https://learn.microsoft.com/pt-br/dotnet/api/system.collections.generic?view=net-8.0) (30 min)

## 3. Programação Orientada a Objetos em C# (3h)

POO, ou Programação Orientada a Objetos, é um paradigma de programação que organiza o software em torno de "objetos", que são instâncias de "classes". Cada objeto representa uma entidade do mundo real ou conceitual com características (atributos) e comportamentos (métodos).

- [[Documentação] - Classes](https://learn.microsoft.com/pt-br/dotnet/csharp/fundamentals/tutorials/classes) (30 min)

- [[Documentação] - POO](https://learn.microsoft.com/pt-br/dotnet/csharp/fundamentals/tutorials/oop) (30 min)

- [[Documentação] - Interface](https://learn.microsoft.com/pt-br/dotnet/csharp/language-reference/keywords/interface) (30 min)

- [[Documentação] - Herança](https://learn.microsoft.com/pt-br/dotnet/csharp/fundamentals/tutorials/inheritance) (30 min)

- [[Tutoriais] - Curso de POO](https://www.youtube.com/watch?v=wI6zLJJhnNw&list=PLWXw8Gu52TRKlAqSfkdhSTPtAfAcYko5E&index=1) (1h)


## 4. Programação Assíncrona - (1h)
Programação Assíncrona é um modelo de programação que permite a execução de operações sem bloquear a execução de um programa. Em vez de esperar que uma operação, como uma solicitação de rede ou leitura de arquivo, seja concluída, a programação assíncrona permite que o programa continue executando outras tarefas, melhorando a eficiência e a capacidade de resposta.

- [[Documentação] - Programação Assíncrona](https://learn.microsoft.com/pt-br/dotnet/csharp/asynchronous-programming/) (1h)


## 5. Injeção de dependência (45 min)

**Injeção de Dependência (Dependency Injection - DI)** é um padrão de design usado para implementar o princípio da inversão de controle (IoC), que tem como objetivo tornar um sistema de software mais flexível, modular e fácil de testar. 

- [[Documentação] - Injeção de dependência](https://www.macoratti.net/20/05/c_consoledi2.htm) (30 min)
  
- [[Documentação] - Injeção de dependência](https://medium.com/@eduardolanfredi/inje%C3%A7%C3%A3o-de-depend%C3%AAncia-ff0372a1672) (15 min)


## 6. Docker e Docker-Compose (2h)

**Docker** é uma plataforma de contêineres que permite empacotar, distribuir e executar aplicativos de maneira isolada e consistente em diferentes ambientes. Um contêiner Docker encapsula tudo o que um software precisa para ser executado, incluindo o código, as bibliotecas, as dependências e o sistema operacional, garantindo que ele funcione da mesma maneira em qualquer lugar.

**Docker-Compose** é uma ferramenta que facilita a definição e execução de múltiplos contêineres Docker como parte de um único aplicativo. Ele usa um arquivo YAML (docker-compose.yml) para definir serviços, redes e volumes, permitindo a execução de aplicativos compostos por várias partes (como servidores web, bancos de dados, filas de mensagens, etc.) com um único comando.

- [[Vídeo Aula] - Curso sobre Docker](https://www.youtube.com/watch?v=pg19Z8LL06w) (1h)
  
- [[Vídeo Aula] - Curso sobre Docker Compose](https://www.youtube.com/watch?v=SXwC9fSwct8) (1h)


## 7. Introdução ao .NET (2:15h)

.NET é uma plataforma de desenvolvimento de software criada pela Microsoft que fornece um ambiente para a criação, execução e manutenção de aplicações. Ela suporta diversas linguagens de programação, como C#, F#, e Visual Basic, e permite o desenvolvimento de uma ampla variedade de aplicações, incluindo aplicativos web, desktop, móveis, de nuvem, jogos e IoT (Internet das Coisas).

- [[Vídeo Aula] - Curso sobre .NET (teórico)](https://www.youtube.com/watch?v=hlgm_1Bzt-4) (15 min)

- [[Documentação] - Minimal Api](https://learn.microsoft.com/pt-br/aspnet/core/fundamentals/minimal-apis/overview?view=aspnetcore-8.0) (1h)

- [[Tutoriais] - ASP.NET Core MVC + EF Core](https://learn.microsoft.com/pt-br/aspnet/core/data/ef-mvc/?view=aspnetcore-8.0) (1h)

**Cursos complementares (2:50h):**
- [[Vídeo Aula] - .NET 8 Web API & Entity Framework](https://www.youtube.com/watch?v=b8fFRX0T38M) (50 min)

- [[Vídeo Aula] - Criando API em ASP.NET 8 com Minimal APIs ](https://www.youtube.com/watch?v=mLceZ0j5MVQ) (43 min)

- [[Vídeo Aula] - CRIANDO UMA CRUD API COM .NET](https://www.youtube.com/watch?v=b7OoeiG_BzU) (1:20 min)


## 8. Introdução ao Clean Architecture (3:45h)

Clean Architecture é um conceito de arquitetura de software proposto para organizar o código de forma que seja fácil de entender, manter e evoluir. A Clean Architecture visa a criação de sistemas modulares, altamente coesos, de baixo acoplamento e independentes de frameworks, interfaces de usuário, bancos de dados e outros detalhes de implementação.

- [[Vídeo Aula] - Curso sobre Clean Architecture (teórico)](https://www.youtube.com/watch?v=ow8UUjS5vzU) (15 min)

- [[Vídeo Aula] - Setup de um projeto com Clean Architecture](https://www.youtube.com/watch?v=ow8UUjS5vzU) (15 min)

- [[Tutoriais] - Curso sobre Clean Architecture (prático)](https://www.youtube.com/watch?v=ZWfrI5Bu6so&list=PLJ4k1IC8GhW3GICba2dLmiTZrVPw0SthC) (assistir do 7 vídeo em diante) (1:30h)


### Auto Mapper (30 min)
AutoMapper é uma biblioteca em .NET que facilita o mapeamento de objetos de um tipo para outro.

- [[Artigo] - Auto Mapper com .Net Core](https://glerystonmatos.medium.com/auto-mapper-com-net-core-9d09856059b3) (15 min)

- [[Artigo] - ASP .NET Core - Usando o AutoMapper](https://www.macoratti.net/19/04/aspc_automap2.htm) (15 min)

### Mediator (20 min)
Mediator é um padrão de design que facilita a comunicação entre diferentes partes de um sistema sem que elas se comuniquem diretamente. O MediatR é usado para enviar comandos e consultas, de modo que cada comando ou consulta tenha apenas um manipulador responsável por processá-los.

- [[Artigo] - Mediator Pattern](https://medium.com/codenx/mediator-pattern-explained-with-c-f0562cbce880) (10 min)

- [[Artigo] - Mediator Pattern](https://dev.to/kalkwst/mediator-pattern-in-c-4i3p) (10 min)

### Unit Of Work (20 min)
Unit of Work (Unidade de Trabalho) é um padrão que mantém um registro das operações realizadas em um repositório durante uma transação. Ele coordena a escrita de mudanças para a base de dados, garantindo que todas as operações sejam bem-sucedidas ou nenhuma delas será persistida.

- [[Vídeo Aula] - Unit Of Work Patterns](https://www.youtube.com/watch?v=vN_j1Bs0ALU&list=PLYpjLpq5ZDGv370qMB4PLF-PlGdBhP0PA) (10 min)

- [[Artigo] - Unit Of Work Patterns](https://www.codeproject.com/Articles/5377618/Unit-of-Work-Pattern-in-Csharp-for-Clean-Architect) (10 min)


### Repository Patterns (10 min)
Repository Pattern (Padrão de Repositório) é um padrão de design que abstrai a lógica de acesso a dados. Ele fornece uma interface para realizar operações de CRUD (Create, Read, Update, Delete) e oculta os detalhes de implementação do armazenamento de dados (por exemplo, Entity Framework).

- [[Vídeo Aula] - Repository Patterns](https://www.youtube.com/watch?v=h4KIngWVpfU&list=PLYpjLpq5ZDGsQUN89adlTUFtmT1q6-YW3) (10 min)

### Generic Repository (10 min)
Generic Repository é uma implementação do Repository Pattern que é genérica, permitindo que a lógica de acesso a dados seja reutilizada para diferentes tipos de entidades.

- [[Vídeo Aula] - Generic Repository](https://www.youtube.com/watch?v=Bz5JCbWnaHo&list=PLYpjLpq5ZDGsQUN89adlTUFtmT1q6-YW3) (10 min)

### Result Pattern (10 min)
Result Pattern é um padrão utilizado para representar o resultado de uma operação, especialmente em termos de sucesso ou falha. Geralmente, um objeto Result contém informações sobre se a operação foi bem-sucedida, uma mensagem de erro (se aplicável), e quaisquer dados adicionais relevantes.

- [[Vídeo Aula] - Result Pattern](https://www.youtube.com/watch?v=WCCkEe_Hy2Y) (10 min)

### CQRS (1:15h)
CQRS é um padrão de arquitetura que separa operações de leitura (Queries) das operações de escrita (Commands) em sistemas. Em vez de usar o mesmo modelo de dados para ler e escrever, CQRS promove a criação de modelos distintos para cada um.

- [[Curso] - CQRS](https://www.youtube.com/watch?v=xnbLwL_OzNE&list=PLJ4k1IC8GhW0lLT4Hs2rD8veMJyLGbrJ3&pp=iAQB) (1h)

- [[Artigo] - CQRS](https://balta.io/blog/aspnet-core-cqrs-mediator) (15 min)

### Autênticação JWT (45 min)
JWT (JSON Web Token) é um padrão para representar declarações de forma segura entre duas partes. Em sistemas de autenticação, JWT é usado para transmitir informações de forma compacta e segura, como a identidade do usuário e as permissões associadas a ele.

- [[Curso] - Token Authentication In ASP.NET Core](https://www.youtube.com/watch?v=4cFhYUK8wnc) (25 min)

- [[Curso] -  Create JSON Web Tokens (JWT) - User Registration / Login / Authentication](https://www.youtube.com/watch?v=UwruwHl3BlU) (20 min)

**Leitura Complementar (35 min):**

- [[Artigo] - ASP.NET 8 - Authentication and Authorization in 7 steps.](https://dev.to/vinicius_estevam/aspnet-8-authentication-and-authorization-3426/) (35 min)

- [[Artigo] - Clean Architecture in ASP .NET Core Web API](https://medium.com/@mohanedzekry/clean-architecture-in-asp-net-core-web-api-d44e33893e1d) (10 min)

## 9. Testes no ambiente .NET (2:30h)

Testes unitários, BDD (Behavior-Driven Development) e handlers são práticas fundamentais no desenvolvimento de software moderno para garantir qualidade, manutenibilidade e colaboração eficaz. Testes unitários verificam pequenas unidades de código isoladamente para detectar erros precocemente e facilitar refatorações seguras. 

- [[Tutoriais] - Unit Testing in C# 2022](https://www.youtube.com/watch?v=a6Qab5l-VLo) (2h)

- [[Vídeo Aula] - Unit Testing CQRS Handlers With Moq, Fluent Assertions, and xUnit](https://www.youtube.com/watch?v=a6Qab5l-VLo) (15 min)
  
- [[Artigo] - BDD Testing in .NET8](https://dev.to/vinicius_estevam/teste-bdd-em-net8-4ech) (15 min)